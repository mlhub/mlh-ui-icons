import Vue from "vue";

export default Vue.extend({
    name: 'vue-svg-filler',
    render(c: any) {
        return c('svg', {
            style: {'vertical-align': 'middle'},
            attrs: {
                // @ts-ignore
                viewBox: this.viewBox,
                // @ts-ignore
                width: this.width,
                // @ts-ignore
                height: this.height,
            },
        },)
    },
    props: {
        path: {
            type: String,
            required: true
        },
    },
    data() {
        return {
            viewBox: '0 0 24 24',
            width: 24,
            height: 24,
        };
    },
    watch: {
        path(): void {
            this.createSvgElement();
        },
    },
    methods: {
        createSvgElement(): void {
            let dir = window.location.origin
            let source = this.path.match(/^http(s?):\/\//) ? this.path : (this.path.substring(0, 1) === '/' ? `${dir}${this.path}` : `${dir}/${this.path}`)
            let request = new XMLHttpRequest()
            request.open('GET', source, true)
            request.onload = () => {
                if (request.status >= 200 && request.status < 400) {
                    let domParser = new DOMParser();
                    let elementSvg = domParser.parseFromString(request.responseText, 'text/xml');
                    let tagSvg = elementSvg.getElementsByTagName('svg')[0];

                    let svgElement = this.$vnode.elm;
                    // @ts-ignore
                    let viewBox = svgElement.viewBox.baseVal;

                    setTimeout(() => {
                        this.width = viewBox.width;
                        this.height = viewBox.height;
                    }, 1);

                    // @ts-ignore
                    svgElement.innerHTML = tagSvg.innerHTML;
                    // @ts-ignore
                    this.viewBox = tagSvg.getAttribute('viewBox');
                } else {
                    this._errorLog(`Can't load element from this path.\nPath : ${source}`);
                }
            }
            request.onerror = () => {
                this._errorLog(`Can't load element from this path.\nPath : ${source}`);
            }
            request.send()
        },
        _errorLog(log: string): any {
            console.error(`[ERROR] : vue-svg-filler, ${log}`);
        },
    },
    mounted() {
        this.createSvgElement();
    },
});
